package co.softeng.alex.amzscout;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Aspect
@Configuration
public class LimitRateAspect {

    private static final int REQUESTS_PER_MINUTE = 50;

    private static final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private static final Map<String, AtomicInteger> monitors = new ConcurrentHashMap<>();

    private static Logger log = LoggerFactory.getLogger(LimitRateAspect.class);

    @Autowired
    private HttpServletRequest request;

    @Autowired
    private HttpServletResponse response;

    @Around("@annotation(co.softeng.alex.amzscout.LimitRate)")
    public Object profile(ProceedingJoinPoint pjp) throws Throwable {

        String remoteIpAddressOrLastProxy = request.getRemoteAddr();

        AtomicInteger counter = monitors.get(remoteIpAddressOrLastProxy);
        int count = counter != null ? counter.get() : 0;

        if (count > REQUESTS_PER_MINUTE) {
            response.sendError(502);
            return null;
        } else {
            incrementAndScheduleCountdown(remoteIpAddressOrLastProxy, 1, TimeUnit.MINUTES);
            return pjp.proceed();
        }
    }

    private static int incrementAndScheduleCountdown(String remoteAddress, Integer delay, TimeUnit unit) {

        AtomicInteger result = monitors.compute(remoteAddress, (key, counter) -> {

            if (counter == null) {
                log.debug("Init count for {}", key);
                return new AtomicInteger(1);
            } else {
                counter.incrementAndGet();
                log.trace("Increment count for {} to {}", key, counter);
                return counter;
            }
        });

        scheduler.schedule(() -> countdown(remoteAddress), delay, unit);

        return result.get();
    }

    private static void countdown(String remoteAddress) {

        monitors.computeIfPresent(remoteAddress, (key, counter) -> {
            log.trace("Decrement count for {} to {}", key, counter);
            if (counter.decrementAndGet() < 1) {
                log.debug("Disposing counter for {}", key);
                return null;
            } else return counter;
        });
    }
}