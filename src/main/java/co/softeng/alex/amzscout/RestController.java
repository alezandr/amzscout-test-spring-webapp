package co.softeng.alex.amzscout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
class RestController {

    @LimitRate
    @GetMapping("/test")
    @ResponseBody
    public String test() {
        return "";
    }
}
