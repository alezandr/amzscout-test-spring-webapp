package co.softeng.alex.amzscout;

import org.springframework.boot.SpringApplication;

public class Runner {

    public static void main(String[] args) {
        SpringApplication.run(AppConfig.class, args);
    }
}
